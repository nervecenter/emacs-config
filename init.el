;; init.el
;; by Chris Collazo

;; PACKAGE ARCHIVES

(require 'package)

(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa-stable" . "https://stable.melpa.org/packages/")))

(package-initialize)

;; Add downloaded folder to load-path for manually extracted packages
(add-to-list 'load-path "~/.emacs.d/downloaded")

;; Enable packages and modes
(require 'evil)
(evil-mode 1)

(require 'parinfer-mode)


(require 'flx-ido)
(ido-mode 1)
(ido-everywhere 1)
(ido-vertical-mode 1)
(flx-ido-mode 1)
;; disable ido faces to see flx highlights.
(setq ido-enable-flex-matching t)
(setq ido-use-faces nil)

(require 'recentf)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 100)

(require 'helm)
(require 'helm-swoop)
;(global-set-key (kbd "C-x b") 'helm-mini)

(defun half-split-toggle ()
  (interactive)
  (if (>= 2 (count-windows))
    (delete-other-windows)
    (split-window-right)))

(defun kill-this-buffer ()
  (interactive)
  (kill-buffer (current-buffer)))

(require 'evil-leader)
(evil-leader/set-leader "<SPC>")
(evil-leader/set-key
  "f" 'find-file
  "b" 'helm-mini
  "k" 'kill-buffer
  "s" 'helm-swoop
  "g" 'helm-do-grep
  "d" 'half-split-toggle)
(global-evil-leader-mode)

(add-hook 'package-mode-hook 'evil-mode)
